# FROM node:6.10.0-alpine
FROM hypriot/rpi-node:6.10.0-alpine

RUN mkdir -p /app/dist
WORKDIR /app

COPY ["./webpack.config.js", "./package.json", "/app/"]
RUN npm install && npm cache clean

EXPOSE 8082
VOLUME /app/src
COPY dist/index.html /app/dist

ENTRYPOINT ["npm"]
CMD ["start"]
