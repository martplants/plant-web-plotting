var express = require('express');
var path = require('path');

var app = express();

// serve our static stuff like index.css
var DIST_PATH = path.join(__dirname, '../../dist');
console.log(DIST_PATH);
app.use(express.static(DIST_PATH));

// send all requests to index.html so browserHistory in React Router works
app.get('*', function (req, res) {
    res.sendFile(DIST_PATH, 'index.html');
});

var PORT = process.env.PORT || 8082;
app.listen(PORT, function() {
    console.log('Production Express server running at localhost:' + PORT)
});
