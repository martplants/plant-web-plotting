import {computed, observable} from "mobx"
import $ from 'jquery'
import { TimeSeries } from "pondjs";

export class MartStore {
    @observable seriesData = {
        "name": "traffic",
        "columns": ["time", "value"],
        "points": [[0, 0]]
    };

    @computed get timeSeriesData() {
        const cols = this.seriesData.columns.slice();
        const pts = this.seriesData.points.map( (element) => {
            return element.slice();
        });
        let index;
        let j;
        let pts_filtered = [];
        const smoothing_length = 5;
        for (index = smoothing_length; index < pts.length - (smoothing_length + 1); ++index) {
            let pt_average = 0;
            for (j = index - smoothing_length; j < index+smoothing_length + 1; ++j) {
                pt_average += parseInt(pts[j][1]);
            }
            pts_filtered.push([pts[index][0], pt_average/(smoothing_length*2+1)]);
        }
        if (pts_filtered.length === 0) pts_filtered = [[0, 0]];
        return new TimeSeries({
            "name": this.seriesData.name,
            "columns": cols,
            "points": pts_filtered
        });
    }

    setPhantData = (public_key) => {
        let points = [];
        $.ajax({
            url: 'http://192.168.1.249:8080/output/' + public_key + '.json',
            data: {page: 1},
            dataType: 'jsonp',
        }).done((results) => {
            $.each(results, (index, row) => {
                let t = new Date(row.timestamp);
                points.push([t.valueOf(), row.moisture]);
            });
            this.seriesData.points.replace(points.reverse());
        });

    }
}

export default new MartStore




















