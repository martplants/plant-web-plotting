import React from "react"
import {Charts, ChartContainer, ChartRow, YAxis, LineChart, Resizable} from "react-timeseries-charts";
import {observer} from "mobx-react"

let public_key = '9WXYN49K1JTqv9V1mqP6SxyewvkQ';

@observer
export default class MartSite extends React.Component {
    componentDidMount() {
        const {setPhantData} = this.props.store;
        setPhantData(public_key)
    }

    render() {
        const {setPhantData, timeSeriesData} = this.props.store;
        return (
            <div>
                <h1>Martins hemsida</h1>
                    <Resizable>
                        <ChartContainer timeRange={timeSeriesData.timerange()}
                                        width={400}
                                        enablePanZoom={true}
                                        showGrid={true}>
                            <ChartRow height="200">
                                <YAxis id="axis1"
                                       label="MOISTURE"
                                       min={parseInt(timeSeriesData.min("value"))}
                                       max={parseInt(timeSeriesData.max("value"))}
                                       type="linear"
                                       format="5d"
                                />
                                <Charts>
                                    <LineChart axis="axis1" series={timeSeriesData}/>
                                </Charts>
                            </ChartRow>
                        </ChartContainer>
                    </Resizable>
                <a href="#" onClick={() => setPhantData(public_key)}>Refresh graph</a>
            </div>
        )
    }
}
