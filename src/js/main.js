import "../css/main.css"
import React from "react"
import ReactDOM from "react-dom"
import MartStore from "./MartStore"
import MartSite from "./MartSite"

const app = document.getElementById("app");

ReactDOM.render(<MartSite store={MartStore} />, app);

