//noinspection ES6ConvertVarToLetConst
var debug = process.env.NODE_ENV !== "production";
//noinspection ES6ConvertVarToLetConst
var webpack = require('webpack');
//noinspection ES6ConvertVarToLetConst
var path = require('path');

//noinspection JSUnresolvedFunction
module.exports = {
    context: path.join(__dirname, "src"),
    devtool: debug ? "inline-sourcemap" : null,
    entry: "./js/main.js",
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react'],
                    plugins: ["transform-decorators-legacy", "transform-class-properties"]
                }

            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"},
        ]
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: debug ? [] : [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({mangle: false, sourcemap: false}),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        })
    ],
};
